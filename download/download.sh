#!/bin/bash
# written by Siegfried Kienzle <siegfried.kienzle@gmx.de>
DOWNLOADLISTE="download_list"

function download_gcc_prerequisites()
{
	gccfoldername="$1"
	echo "Download prerequisites for $gccfoldername/"
	cd "./$gccfoldername/"
	sh "./contrib/download_prerequisites"
	rm *.tar.*
	cd "../"
		
}

function download_file()
{
	downloadLink="$1"
	filename=$(basename "$downloadLink")
	cd "$TOOLSPATH"
	echo "Download $filename into $TOOLSPATH:"
	wget "$downloadLink"
	echo "Extract $filename into $TOOLSPATH"
	tar xf "$filename"
	echo "Delete $filename"
	rm -rf "$filename"	
	filenameWithoutFirstExt="${filename%.*}"
	filenameWithoutExt="${filenameWithoutFirstExt%.*}"
	if [[ $filenameWithoutExt = *"gcc-"* ]]; then
		download_gcc_prerequisites "$filenameWithoutExt"
	fi
	
}

if [ ! -f "$DOWNLOADLISTE" ]; then
	echo "$DOWNLOADLISTE not exist!"
	exit 2
fi

while read link;
do 
	download_file "$link"
done < "$DOWNLOADLISTE"
