#!/bin/bash
# written by Siegfried Kienzle <siegfried.kienzle@gmx.de>

export TOOLSPATH=~/gcc_cross_pi

sudo ./create_gcc_folder/create_gcc_folder.sh

if [ ! -d "$TOOLSPATH" ]; then
	mkdir "$TOOLSPATH"
fi

cd download
./download.sh
./clone_kernel_sources.sh
cd ..
./header_inst_script/header_inst.sh
./create_binutils/create_binutils.sh
