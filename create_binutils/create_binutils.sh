#!/bin/bash
# written by Siegfried Kienzle <siegfried.kienzle@gmx.de>
BINUTILSFOLDERNAME="build-binutils"
PREFIXPATH="/opt/cross-rasp-pi-gcc" 
TARGET="arm-linux-gnueabihf"
WITHARCH="armv6"
WITHFPU="vfp"
WITHFLOAT="hard"
CORES=$(grep -c ^processor /proc/cpuinfo)


cd "$TOOLSPATH"

if [ ! -d "$BINUTILSFOLDERNAME" ]; then
	mkdir "$BINUTILSFOLDERNAME" 
fi
cd "$BINUTILSFOLDERNAME"
../binutils-2.28/configure --prefix="$PREFIXPATH" --target="$TARGET" --with-arch="$WITHARCH" --with-fpu="$WITHFPU" --with-float="$WITHFLOAT" --disable-multilib
make -j"$((CORES*2))"
make install
