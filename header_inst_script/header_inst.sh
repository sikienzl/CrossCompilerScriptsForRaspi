#!/bin/bash
# written by Siegfried Kienzle <siegfried.kienzle@gmx.de>

cd "$TOOLSPATH/linux"
KERNEL=kernel7
make ARCH=arm INSTALL_HDR_PATH="/opt/cross-rasp-pi-gcc/arm-linux-gnueabihf" headers_install
