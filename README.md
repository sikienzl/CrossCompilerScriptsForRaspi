# CrossCompilerScriptsForRaspi

## Overview

| Software | Version | working? |
|:--------:|:-------:|:--------:|
| binutils |   2.28  | &#x2714; |
| gcc      |  6.3.0  | &#x274C; |
| glibc    |   2.24  | &#x274C; |
| gcc      |  8.1.0  | &#x274C; |

# Requirements
If you want to execute these scripts, you need the packages git, build-tools (gcc, make, etc.) and wget.

## arch
```sudo pacman -Sy base-devel git wget```

## debian/ubuntu
```sudo apt-get install build-essential git wget```

## Install of base CrossTools for Raspi

Run:
```./cross_pi.sh```
