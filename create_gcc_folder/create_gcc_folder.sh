#!/bin/bash
# written by Siegfried Kienzle <siegfried.kienzle@gmx.de>

CROSSRASPIPATH="/opt/cross-rasp-pi-gcc"

if [[ $EUID -ne 0 ]]; then
	echo "This script must be run as root"
	exit 1
fi

if [ ! -d "$CROSSRASPIPATH" ]; then
	echo "Create $CROSSRASPIPATH"
	mkdir -p "$CROSSRASPIPATH"
	chown root:users "$CROSSRASPIPATH"
	chmod 770 "$CROSSRASPIPATH"
fi
export PATH="$CROSSRASPIPATH/bin:$PATH"

